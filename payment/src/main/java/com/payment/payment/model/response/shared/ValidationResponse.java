package com.payment.payment.model.response.shared;

import com.payment.payment.base.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValidationResponse extends BaseResponse {

    private Boolean valids;
}
