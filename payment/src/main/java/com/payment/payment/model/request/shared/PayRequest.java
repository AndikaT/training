package com.payment.payment.model.request.shared;

import com.payment.payment.base.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class PayRequest extends BaseRequest {

    private Long orderId;

}
