package com.payment.payment.model.request.shared;


import com.payment.payment.base.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchRequest extends BaseRequest {
    
    private String textSearch;
    private Integer start;
    private Integer limit;
    private String sortBy;
    private String sort;
}
