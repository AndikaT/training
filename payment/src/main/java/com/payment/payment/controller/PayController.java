package com.payment.payment.controller;

import com.payment.payment.model.request.shared.PayRequest;
import com.payment.payment.model.response.shared.RestResponse;
import com.payment.payment.model.response.shared.ValidationResponse;
import com.payment.payment.service.PayService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/v1/payment")
public class PayController extends BaseController{

    private PayService payService;

    public PayController(PayService payService) {
        this.payService = payService;
    }

    @PostMapping(value="/pay/{order-id}")
    public ResponseEntity<RestResponse> paying(@PathVariable("order-id")Long orderId){
        ValidationResponse response = this.payService.execute(PayRequest.builder()
                .orderId(orderId).build());
        return ResponseEntity.ok(RestResponse.builder()
                .data(response)
                .message("yes")
                .result(true)
                .build());
    }
}
