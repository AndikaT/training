package com.payment.payment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "payDetails")
public class PayDetails extends BaseEntity{

    @Column(name="productName")
    private String productName;

    @Column(name="payId")
    private Long payId;

    @ManyToOne
    @JoinColumn(name="payId", insertable = false, updatable = false)
    private Pay pay;

    @Column(name="quantity")
    private Integer quantity;

    @Column(name="price")
    private BigDecimal price;
}
