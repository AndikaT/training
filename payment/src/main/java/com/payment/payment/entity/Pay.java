package com.payment.payment.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "pay")
public class Pay extends BaseEntity{

    @Column(name="name")
    private String name;
    @Column(name="invoiceId")
    private String invoiceId;
    @Column(name="address")
    private String address;
    @Column(name="totalPrice")
    private BigDecimal totalPrice;
    @Column(name="pay")
    private String pay;

}
