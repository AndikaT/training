package com.payment.payment.repository;

import com.payment.payment.entity.PayDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PayDetailsRepository extends JpaRepository<PayDetails,Long>, JpaSpecificationExecutor<PayDetails> {
}
