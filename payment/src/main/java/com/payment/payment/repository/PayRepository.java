package com.payment.payment.repository;

import com.payment.payment.entity.Pay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PayRepository extends JpaRepository<Pay, Long>, JpaSpecificationExecutor<Pay> {

    @Query(value="select MAX(p.id) from pay p",nativeQuery = true)
    List<Long> getMaxId();
}
