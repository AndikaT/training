package com.payment.payment.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.payment.payment.base.BaseService;
import com.payment.payment.entity.Pay;
import com.payment.payment.entity.PayDetails;
import com.payment.payment.model.request.shared.PayRequest;
import com.payment.payment.model.response.shared.ValidationResponse;
import com.payment.payment.repository.PayDetailsRepository;
import com.payment.payment.repository.PayRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
@Service
public class PayService implements BaseService<PayRequest, ValidationResponse> {
    private PayRepository payRepository;
    private PayDetailsRepository payDetailsRepository;
    private RestTemplate restTemplate;

    public PayService(PayRepository payRepository, PayDetailsRepository payDetailsRepository, RestTemplate restTemplate) {
        this.payRepository = payRepository;
        this.payDetailsRepository = payDetailsRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.NESTED)
    public ValidationResponse execute(PayRequest input) {

        HttpHeaders headersGetProductId = new HttpHeaders();
        headersGetProductId.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> testCallGetProductId = new HttpEntity<>(headersGetProductId);
        String response = this.restTemplate.exchange(
                "http://localhost:8080/core/api/v1/order/get-order/"+input.getOrderId(),
                HttpMethod.GET, testCallGetProductId,
                String.class).getBody();

        JSONObject parseJsonString = JSONObject.parseObject(response);
        JSONObject mapDataToJSON = parseJsonString.getJSONObject("data");

        Pay pay = Pay.builder()
                .address(mapDataToJSON.getString("address"))
                .name(mapDataToJSON.getString("name"))
                .invoiceId(mapDataToJSON.getString("invoiceId"))
                .pay("success")
                .build();

        this.payRepository.save(pay);

        JSONArray details = mapDataToJSON.getJSONArray("orderDetailsList");

        for(int i=0 ; details.size()>i ; i++){
            this.payDetailsRepository.save(PayDetails.builder()
                    .price(details.getJSONObject(i).getBigDecimal("price"))
                    .quantity(details.getJSONObject(i).getInteger("quantity"))
                    .productName(details.getJSONObject(i).getJSONObject("products").getString("name"))
                    .payId(this.payRepository.getMaxId().get(0)).build());
        }

        return ValidationResponse.builder()
                .valids(true)
                .build();






    }
}
