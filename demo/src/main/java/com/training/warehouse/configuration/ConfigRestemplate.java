package com.training.warehouse.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigRestemplate {

    @Bean
    public RestTemplate configRestTemplate(){
        System.out.println("this is config rest template..");
        return new RestTemplate();
    }
}
