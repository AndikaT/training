package com.training.order.service;

import com.training.order.base.BaseService;
import com.training.order.entity.Orders;
import com.training.order.entity.OrderDetails;
import com.training.order.entity.orderStuff;
import com.training.order.model.request.shared.NewOrderRequest;
import com.training.order.model.response.shared.NewOrderResponse;
import com.training.order.repository.OrderDetailsRepository;
import com.training.order.repository.OrderRepository;
import com.training.order.repository.ProductsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.NoSuchElementException;

@Slf4j
@Service
public class NewOrderService implements BaseService<NewOrderRequest, NewOrderResponse> {

    private OrderRepository orderRepository;
    
    private ProductsRepository productsRepository;

    private OrderDetailsRepository orderDetailsRepository;

    public NewOrderService(OrderRepository orderRepository, ProductsRepository productsRepository, OrderDetailsRepository orderDetailsRepository) {
        this.orderRepository = orderRepository;
        this.productsRepository = productsRepository;
        this.orderDetailsRepository = orderDetailsRepository;
    }

    @Override
    @Transactional(propagation = Propagation.NESTED)
    public NewOrderResponse execute(NewOrderRequest input) {
        Long identifier = new Date().getTime();



//        input.getOrderDetails().forEach(total = total + this.productsRepository.findById());

//        BigDecimal total_price = new BigDecimal(0);
//        for(orderStuff stuff : input.getOrderDetails()){
//            total_price += this.productsRepository.findById(stuff.getProductsId()).get().getPrice();
//        }



        Orders orders = Orders.builder()
                .name(input.getName())
                .invoiceId(identifier.toString())
                //.totalPrice()
                .build();

        this.orderRepository.save(orders);

        input.getOrderDetails().stream()
                .forEach(cartItem -> this.productsRepository.findById(cartItem.getProductsId())
                        .map(products -> this.orderDetailsRepository.save(OrderDetails.builder()
                                .price(products.getPrice())
                                .orderId(this.orderRepository.getMaxId().get(0))
                                .quantity(cartItem.getQuantity())
                                .productsId(cartItem.getProductsId())
                                .build()))
                        .orElseThrow(()-> new NoSuchElementException("Product id not found " + cartItem.getProductsId())));


        return NewOrderResponse.builder()
                .invoiceId(identifier.toString()).build();
    }
}
