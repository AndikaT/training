package com.training.order.service;

import com.training.order.base.BaseService;
import com.training.order.model.request.shared.GetOrderByIdRequest;
import com.training.order.model.response.shared.GetOrderByIdResponse;
import com.training.order.repository.OrderDetailsRepository;
import com.training.order.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Slf4j
@Service
public class GetOrderByIdService implements BaseService<GetOrderByIdRequest, GetOrderByIdResponse> {

    private OrderDetailsRepository orderDetailsRepository;

    private OrderRepository orderRepository;

    public GetOrderByIdService(OrderDetailsRepository orderDetailsRepository, OrderRepository orderRepository) {
        this.orderDetailsRepository = orderDetailsRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public GetOrderByIdResponse execute(GetOrderByIdRequest input) {
        GetOrderByIdResponse response = this.orderRepository.findById(input.getOrderId()).map(
                order -> GetOrderByIdResponse
                        .builder()
                        .name(order.getName())
                        .address(order.getAddress())
                        .invoiceId(order.getInvoiceId())
                        .orderDetailsList(this.orderDetailsRepository.getDetailsByOrderId(input.getOrderId()))
                        .build()
        ).orElseThrow(()-> new NoSuchElementException("Order dengan ID: " + input.getOrderId() + " tidak ditemukan"));

        return response;
    }
}
