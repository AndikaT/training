package com.training.order.utility;

import com.training.order.configuration.RedisConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.InvalidURIException;

import java.net.URI;

@Slf4j
@Component
@RequiredArgsConstructor
public class CacheUtility implements InitializingBean{
    private final RedisConfig redisConfig;

    private Jedis jedis;

    @Override
    public void afterPropertiesSet() {
        String uriConnection = String.format("redis://%s:%s@%s:%s/%s", redisConfig.getUsername(), redisConfig.getPassword(), redisConfig.getHost(), redisConfig.getPort(), redisConfig.getDb());
        if (StringUtils.isEmpty(redisConfig.getUsername()) && StringUtils.isEmpty(redisConfig.getPassword())) {
            uriConnection = String.format("redis://%s:%s/%s", redisConfig.getHost(), redisConfig.getPort(), redisConfig.getDb());
        }
        log.info("Redis URI : {}", uriConnection);
        try {
            this.jedis = new Jedis(URI.create(uriConnection));
        } catch (InvalidURIException e) {
            log.error("Redis URI not available");
        }
    }


    public void set(String prefix, String key, String value, Integer expiration) {
        jedis.connect();
        String pair = createPair(prefix, key);
        jedis.set(pair, value);
        if (ObjectUtils.isNotEmpty(expiration)) {
            jedis.expire(pair, expiration);
        }
        jedis.close();
    }

    public void delete(String prefix, String key) {
        jedis.connect();
        String pair = createPair(prefix, key);
        jedis.del(pair);
        jedis.close();
    }

    public String get(String prefix, String key) {
        jedis.connect();
        String pair = createPair(prefix, key);
        String value = jedis.get(pair);
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        jedis.close();
        return value;
    }

    private String createPair(String prefix, String key) {
        return prefix + redisConfig.getSeparator() + key;
    }

}
