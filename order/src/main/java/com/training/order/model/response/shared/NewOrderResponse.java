package com.training.order.model.response.shared;

import com.training.order.base.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewOrderResponse extends BaseResponse {

    private String invoiceId;

}
