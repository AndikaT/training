package com.training.order.model.request.shared;

import com.training.order.base.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetOrderByIdRequest extends BaseRequest {

    private Long orderId;

}
