package com.training.order.model.response.shared;

import com.training.order.base.BaseResponse;
import com.training.order.entity.OrderDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetOrderByIdResponse extends BaseResponse {

    private String name;
    private String invoiceId;
    private String address;
    private BigDecimal total_price;
    private List<OrderDetails> orderDetailsList;

}
