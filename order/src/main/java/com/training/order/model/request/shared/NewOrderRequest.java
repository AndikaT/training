package com.training.order.model.request.shared;

import com.training.order.base.BaseRequest;
import com.training.order.entity.OrderDetails;
import com.training.order.entity.orderStuff;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewOrderRequest extends BaseRequest {

    @NotBlank(message = "the name must be filled")
    private String name;

    @NotBlank(message = "address must be filled")
    private String address;

    @NotEmpty(message = "at least 1 item included")
    private List<orderStuff> orderDetails;

}


