package com.training.order.repository;

import com.training.order.entity.OrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Long>, JpaSpecificationExecutor<OrderDetails> {

    @Query(value="SELECT * From orderdetails o WHERE o.order_id = ?1",nativeQuery = true)
    List<OrderDetails> getDetailsByOrderId(Long orderId);
}
