package com.training.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="orders")
public class Orders extends BaseEntity{

    @Column(name="name")
    private String name;
    @Column(name="invoiceId")
    private String invoiceId;
    @Column(name="address")
    private String address;
    @Column(name="totalPrice")
    private BigDecimal totalPrice;

}
