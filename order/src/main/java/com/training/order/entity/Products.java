package com.training.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="products")
public class Products extends BaseEntity{

    @Column(name="name")
    private String name;

    @Column(name="stocks")
    private Integer stocks;

    @Column(name="price")
    private BigDecimal price;

}
