package com.training.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orderdetails")
public class OrderDetails extends BaseEntity{
    @Column(name="productsId")
    private Long productsId;

    @ManyToOne
    @JoinColumn(name="productsId", insertable = false, updatable = false)
    private Products products;

    @Column(name="orderId")
    private Long orderId;

    @ManyToOne
    @JoinColumn(name="orderId", insertable = false, updatable = false)
    private Orders orders;

    @Column(name="quantity")
    private Integer quantity;

    @Column(name="price")
    private BigDecimal price;
}
