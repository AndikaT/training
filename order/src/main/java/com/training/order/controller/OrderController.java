package com.training.order.controller;

import com.training.order.model.request.shared.GetOrderByIdRequest;
import com.training.order.model.request.shared.NewOrderRequest;
import com.training.order.model.response.shared.GetOrderByIdResponse;
import com.training.order.model.response.shared.NewOrderResponse;
import com.training.order.model.response.shared.RestResponse;
import com.training.order.service.GetOrderByIdService;
import com.training.order.service.NewOrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "api/v1/order")
public class OrderController extends BaseController{

    private NewOrderService newOrderService;

    private GetOrderByIdService getOrderByIdService;

    public OrderController(NewOrderService newOrderService, GetOrderByIdService getOrderByIdService) {
        this.newOrderService = newOrderService;
        this.getOrderByIdService = getOrderByIdService;
    }

    @PostMapping(value="/add-to-cart")
    public ResponseEntity<RestResponse> newOrder(@Valid @RequestBody NewOrderRequest newOrderRequest){
        NewOrderResponse newOrderResponse = this.newOrderService.execute(newOrderRequest);

        return ResponseEntity.ok(RestResponse.builder()
                .data(newOrderResponse)
                .message("OK")
                .result(true)
                .build());
    }

    @GetMapping(value="/get-order/{id}")
    public ResponseEntity<RestResponse> getOrderById(@PathVariable("id")Long id){
        GetOrderByIdResponse response = this.getOrderByIdService.execute(GetOrderByIdRequest.builder()
                .orderId(id).build());

        return ResponseEntity.ok(RestResponse.builder()
                .data(response)
                .message("OK")
                .result(true)
                .build());
    }
}
